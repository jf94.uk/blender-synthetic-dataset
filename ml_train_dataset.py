import tensorflow as tf
#import tensorflow_datasets as tfds

import csv
import pathlib

dec_row = lambda enc_row: (
                enc_row[:1][0],
                list(
                    map(
                        lambda r: float(r),
                        enc_row[1:]
                    )
                )
            )


def read_csv_data(f_name):
    with open(f_name) as f:
        reader = csv.reader(f)

        return list(
            map(
                lambda r: dec_row(r),
                reader
            )
        )


rows = read_csv_data("./dataset/training/data.csv")
#print(len(rows[0][1]))


"""
list(map(
    lambda r: r[0],
    rows
))
"""


data_dir = pathlib.Path("./dataset/")

image_count = len(list(data_dir.glob('*.jpeg')))

#print(image_count)



batch_size = 32
img_height = 256
img_width = 256


#print(data_dir)

train_ds = tf.keras.utils.image_dataset_from_directory(
    data_dir,
    validation_split=0.2,
    subset="training",
    seed=123,
    image_size=(img_height, img_width),
    batch_size=batch_size
)


print(train_ds.class_names)



"""
for image_batch, labels_batch in train_ds:
    print(image_batch.shape)
    print(labels_batch.shape)
    break
"""
